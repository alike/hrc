# hrc

Hadron-Resonance correlation

# pp phi meson

## @ 2.76 TeV 
| Name | Data - LHC11a_pass4_without_SDD| MC - LHC12f1a_woSDD |
| ------ | -------- | ------ | 
| phi_leading_1.5s_MB | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1420_20190731-1437/phi_leading_1_5s_MB/AnalysisResults.root) | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1249_20190731-1441/phi_leading_1_5s_MB/AnalysisResults.root) |
| phi_leading_2s_MB | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1420_20190731-1437/phi_leading_2s_MB/AnalysisResults.root) | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1249_20190731-1441/phi_leading_2s_MB/AnalysisResults.root) |
| phi_leading_2.5s_MB | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1420_20190731-1437/phi_leading_2_5s_MB/AnalysisResults.root) | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1249_20190731-1441/phi_leading_2_5s_MB/AnalysisResults.root) |
| phi_leading_3s_MB | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1420_20190731-1437/phi_leading_3s_MB/AnalysisResults.root) | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1249_20190731-1441/phi_leading_3s_MB/AnalysisResults.root) |

## @ 5.02 TeV 
| Name | Data - LHC17pq | MC - LHC16k5ab |
| ------ | -------- | ------ | 
| phi_leading_3s_MB | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1723_20200626-1031/phi_leading_3s_MB/AnalysisResults.root) | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1464_20200701-1604_child_1/phi_leading_3s_MB/AnalysisResults.root) |
| phi_leading_3s_INT7 | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1779/phi_leading_3s_INT7/AnalysisResults.root) | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1464_20200701-1604_child_1/phi_leading_3s_MB/AnalysisResults.root) |

## @ 13 TeV 
| Name | Data  - LHC15f_pass2 | MC -  LHC15g3a3 |
| ------ | -------- | ------ | 
| phi_leading_1.5s_MB | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1421_20190731-1439/phi_leading_1_5s_MB/AnalysisResults.root) | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1250_20190731-1441/phi_leading_1_5s_MB/AnalysisResults.root) |
| phi_leading_2s_MB | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1421_20190731-1439/phi_leading_2s_MB/AnalysisResults.root) | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1250_20190731-1441/phi_leading_2s_MB/AnalysisResults.root) |
| phi_leading_2.5s_MB | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1421_20190731-1439/phi_leading_2_5s_MB/AnalysisResults.root) | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1250_20190731-1441/phi_leading_2_5s_MB/AnalysisResults.root) |
| phi_leading_3s_MB | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1421_20190731-1439/phi_leading_3s_MB/AnalysisResults.root) | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1250_20190731-1441/phi_leading_3s_MB/AnalysisResults.root) |

# pp K* meson

## @ 2.76 TeV 
| Name | Data - LHC11a_pass4_without_SDD| MC - LHC12f1a_woSDD |
| ------ | -------- | ------ | 
| k_leading_3s_MB | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1420_20190731-1437/k_leading_3s_MB/AnalysisResults.root) | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1249_20190731-1441/k_leading_3s_MB/AnalysisResults.root) |

## @ 5.02 TeV 
| Name | Data - LHC15n_pass3 | MC - LHC16k5ab |
| ------ | -------- | ------ | 
| k_leading_1.5s_MB | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1537_20191016-1631/k_leading_1_5s_INT7/AnalysisResults.root) | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1380_20191004-1015_child_2/k_leading_1_5s_INT7/AnalysisResults.root) |
| k_leading_2s_MB | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1537_20191016-1631/k_leading_2s_INT7/AnalysisResults.root) | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1380_20191004-1015_child_2/k_leading_2s_INT7/AnalysisResults.root) |
| k_leading_2.5s_MB | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1537_20191016-1631/k_leading_2_5s_INT7/AnalysisResults.root) | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1380_20191004-1015_child_2/k_leading_2_5s_INT7/AnalysisResults.root) |
| k_leading_3s_MB | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1537_20191016-1631/k_leading_3s_INT7/AnalysisResults.root) | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1380_20191004-1015_child_2/k_leading_3s_INT7/AnalysisResults.root) |

| Name | Data - LHC17pq | MC - LHC16k5ab |
| ------ | -------- | ------ | 
| k_leading_3s_INT7 | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1754/k_leading_3s_INT7/AnalysisResults.root)  |  [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1380_20191004-1015_child_2/k_leading_3s_INT7/AnalysisResults.root)
|                   |   [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1804/k_leading_3s_INT7/AnalysisResults.root)|
| k_leading_3s_MB | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1721/k_leading_3s_MB/AnalysisResults.root)  |  [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1380_20191004-1015_child_2/k_leading_3s_INT7/AnalysisResults.root)

## @ 13 TeV 
| Name | Data  - LHC15f_pass2 | MC -  LHC15g3a3 |
| ------ | -------- | ------ | 
| k_leading_3s_MB | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/1430_20190805-1710/k_leading_3s_MB/AnalysisResults.root) | [Link](root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp_MC/1251_20190805-1726/k_leading_3s_MB/AnalysisResults.root) |

# PbPb phi meson

## AOD @ 5.02 TeV
| Name | Data - LHC18qr_pass3 | MC - LHC20g14abc |
| ------ | -------- | ----------------- |
| phi_leading_3s_all (old) | [Link](root://eos.ndmspc.io//eos/ndmspc/scratch/alice.cern.ch/user/a/alitrain/PWGLF/LF_PbPb/1183/phi_leading_3s_semicentral/AnalysisResults.root) | [Link](root://eos.ndmspc.io//eos/ndmspc/scratch/alice.cern.ch/user/a/alitrain/PWGLF/LF_PbPb_MC_AOD/755/phi_leading_3s_all/AnalysisResults.root) |
| phi_leading_2s_all | [Link](root://eos.ndmspc.io//eos/ndmspc/scratch/alice.cern.ch/user/a/alitrain/PWGLF/LF_PbPb_AOD/1257/phi_leading_2s_all/AnalysisResults.root) |
| phi_leading_4s_all | [Link](root://eos.ndmspc.io//eos/ndmspc/scratch/alice.cern.ch/user/a/alitrain/PWGLF/LF_PbPb_AOD/1257/phi_leading_4s_all/AnalysisResults.root) |
| phi_leading_3s_all | [Link](root://eos.ndmspc.io//eos/ndmspc/scratch/alice.cern.ch/user/a/alitrain/PWGLF/LF_PbPb_AOD/1257/phi_leading_3s_all/AnalysisResults.root) |

## @ 2.76 TeV 
| Name | Data - LHC11h_pass2 |
| ------ | -------- |  
| phi_leading_3s_INT7 | [Link](root://eos.ndmspc.io//eos/ndmspc/scratch/alice.cern.ch/user/a/alitrain/PWGLF/LF_PbPb/1619/phi_leading_3s_INT7/AnalysisResults.root)|

## @ 5 TeV
| Name | Data - LHC18pq_pass3 |
| ------ | -------- |
| phi_leading_3s_INT7 | [Link](root://eos.ndmspc.io//eos/ndmspc/scratch/alice.cern.ch/user/a/alitrain/PWGLF/LF_PbPb/1626/phi_leading_3s_INT7/AnalysisResults.root)|
| phi_leading_3s_INT7 (with deta)| [Link](root://eos.ndmspc.io//eos/ndmspc/scratch/alice.cern.ch/user/a/alitrain/PWGLF/LF_PbPb/1659_1/phi_leading_3s_INT7/AnalysisResults.root)|

# PbPb K* meson

## AOD @ 5 TeV
| Name | Data - LHC18pq_pass3 | MC - LHC20g14abc |
| ------ | ---------------- | ------------ |
| k_star_leading_3s_INT7 (mixing)| [Link](root://eos.ndmspc.io//eos/ndmspc/scratch/alice.cern.ch/user/a/alitrain/PWGLF/LF_PbPb_AOD/1189/k_star_leading_3s_INT7/AnalysisResults.root)| [Link](root://eos.ndmspc.io//eos/ndmspc/scratch/alice.cern.ch/user/a/alitrain/PWGLF/LF_PbPb_MC_AOD/756/k_star_leading_3s_all/AnalysisResults.root)|
| k_star_leading_3s_INT7 (likesign) | [Link](root://eos.ndmspc.io//eos/ndmspc/scratch/alice.cern.ch/user/a/alitrain/PWGLF/LF_PbPb/1165/k_star_leading_3s_INT7/AnalysisResults.root)| 

## @ 5 TeV
| Name | Data - LHC18pq_pass3 |
| ------ | -------- |
| k_star_leading_3s_INT7 | [Link](root://eos.ndmspc.io//eos/ndmspc/scratch/alice.cern.ch/user/a/alitrain/PWGLF/LF_PbPb/1660_2/k_star_leading_3s_INT7/AnalysisResults.root)| 

