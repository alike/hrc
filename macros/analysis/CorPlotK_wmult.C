// 2019-10-08

void CorPlotK_wmult(TString n="dphi[-1.57,-1.22]" )
{
   //gROOT->SetStyle("Plain");
   //gStyle->SetPalette(1);
   //gStyle->SetOptStat(1);

   TFile *f = TFile::Open("K_star/13TeV/mult_LHC15f_pass2-LHC15g3a3-results-final.root","READ");
     if  (!f) return;

    TFolder *root = (TFolder*) f->Get("test");
    if (!root) {
	Printf("root folder was not found!!!");
        return;
    }

   TList *l = new TList();

    TH1 *h;
    TObjArray *arr = n.Tokenize(" ");
    TObjString *os;
    TString s;
    for (Int_t i=0;i<arr->GetEntries();i++) {
     os = (TObjString *) arr->At(i);
     s = os->GetString();
	
      l->Add(root->FindObject(TString::Format("mgr/results/LHC15f_pass2-LHC15g3a3/mult/spectra/norm[1.10,1.15]/fit_5[0.750,1.050]/hWidth"))); 
    };

    TCanvas *c = new TCanvas("c", "c", 2000,2000);
       c->Divide(1, 1);
    

    TIter next(l);
    Int_t count=0;
    while ((h = (TH1*) next())) {
      os = (TObjString *) arr->At(count);
      s = os->GetString();

      h->SetStats(0);
    //Double_t k = count +1; 
      c->cd(count+1);
      
      h->GetYaxis()->SetTitle("#Gamma[GeV/c^{2}]");
      h->GetXaxis()->SetTitle("N_{track}");
           h->GetYaxis()->SetRangeUser(0.02,0.06);
      h->SetTitle("Sirka K^{0*} vs. multiplicita");
      h->DrawCopy();
     
        TLine *l2 = new TLine(0.,0.0474,90.,0.0474);
    l2->SetLineColor(6);
    l2->SetLineStyle(1);
    l2->SetLineWidth(4);
    l2->Draw("same");
   TLegend*leg1= new TLegend(0.5,0.5,0.7,0.7);
   leg1->AddEntry(h,"pp","pl");
   leg1->AddEntry(l2,"PDG","l");
   leg1->Draw();

      count++;
    }
}
