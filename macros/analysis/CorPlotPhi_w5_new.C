// 2019-10-08

void CorPlotPhi_w5_new(TString n="dphi[-1.57,-1.22]" )
{
   //gROOT->SetStyle("Plain");
   //gStyle->SetPalette(1);
   //gStyle->SetOptStat(1);

   TFile *f = TFile::Open("phi/5TeV/LHC17pq-LHC16k5_ab-results-final.root","READ");
     if  (!f) return;

    TFolder *root = (TFolder*) f->Get("test");
    if (!root) {
	Printf("root folder was not found!!!");
        return;
    }

   TList *l = new TList();

    TH1 *h;
    TObjArray *arr = n.Tokenize(" ");
    TObjString *os;
    TString s;
    for (Int_t i=0;i<arr->GetEntries();i++) {
     os = (TObjString *) arr->At(i);
     s = os->GetString();
	
      l->Add(root->FindObject(TString::Format("mgr/results/LHC17pq-LHC16k5_ab/dphi/spectra/norm[1.01,1.01]/fit_1[1.010,1.035]/hWidth"))); 
    };

    TCanvas *c = new TCanvas("c", "c", 2000,2000);
       c->Divide(1, 1);
    

    TIter next(l);
    Int_t count=0;
    while ((h = (TH1*) next())) {
      os = (TObjString *) arr->At(count);
      s = os->GetString();

      h->SetStats(0);
    //Double_t k = count +1; 
      c->cd(count+1);
      
      h->GetYaxis()->SetTitle("#Gamma[GeV/c^{2}]");
      h->GetXaxis()->SetTitle("#Delta#varphi");
           h->GetYaxis()->SetRangeUser(0.002,0.006);
      h->SetTitle("Sirka #phi vs. #Delta#varphi ");
      h->DrawCopy();
        TLine *l2 = new TLine(-1.5,0.00426,4.6,0.00426);
    l2->SetLineColor(6);
    l2->SetLineStyle(1);
    l2->SetLineWidth(4);
    l2->Draw("same");
   TLegend*leg1= new TLegend(0.5,0.5,0.7,0.7);
   leg1->AddEntry(h,"pp","pl");
   leg1->AddEntry(l2,"PDG","l");
   leg1->Draw();
      count++;
    }
}
