// 2019-10-08

void CorPlotPhi_m1(TString n="mult[0.00,10.00] mult[10.00,30.00] mult[30.00,50.00] mult[50.00,90.00]" )
{
   //gROOT->SetStyle("Plain");
   //gStyle->SetPalette(1);
   //gStyle->SetOptStat(1);

   TFile *f = TFile::Open("phi/new/1_LHC15f_pass2-LHC15g3a3-results-final.root","READ");
     if  (!f) return;

    TFolder *root = (TFolder*) f->Get("test");
    if (!root) {
	Printf("root folder was not found!!!");
        return;
    }

   TList *l = new TList();

    TH1 *h;
    TObjArray *arr = n.Tokenize(" ");
    TObjString *os;
    TString s;
    for (Int_t i=0;i<arr->GetEntries();i++) {
     os = (TObjString *) arr->At(i);
     s = os->GetString();
	
      l->Add(root->FindObject(TString::Format("mgr/results/LHC15f_pass2-LHC15g3a3/mult_vs_dphi/spectra/%s/norm[1.10,1.15]/fit_2[0.997,1.050]/hMass",s.Data()).Data())); 
    };
   TLegend *legend =new TLegend(0.5,0.5,0.7,0.7);
    TCanvas *c = new TCanvas ("c", "hRawBC", 1500,1500);
    TIter next(l);
    Int_t count=0;
    while ((h = (TH1*) next())) {
      os = (TObjString *) arr->At(count);
      s = os->GetString();

      h->SetStats(0);

      if (c->GetListOfPrimitives()->GetSize())
	h->SetOption("SAME");
     
      else {
      h->GetYaxis()->SetTitle("M_{inv}[GeV/c^{2}]");
      h->GetYaxis()->SetRangeUser(1.01,1.03);
      h->GetXaxis()->SetTitle("#Delta#varphi");
      h->SetTitle("Hmotnost #phi vs. #Delta#varphi ");
      
     	}

      c->GetListOfPrimitives()->Add(h);
	legend->AddEntry(h,s.Data(),"p");
	h->SetMarkerSize(2);
      h->SetMarkerStyle(20);
      h->SetMarkerColor(count+1);
      count++;
    }
       TLine *l2 = new TLine(-1.5,1.019455,4.6,1.019455);
    l2->SetLineColor(6);
    l2->SetLineStyle(1);
    l2->SetLineWidth(4);
     legend->AddEntry(l2,"PDG","l");
    legend->Draw();
    c->Draw();
    l2->Draw("same");

}
